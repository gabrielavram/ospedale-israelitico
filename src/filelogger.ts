import { File } from '@ionic-native/file';
import { Injectable } from '@angular/core';


@Injectable()
export class FileLogger{

  constructor(public file: File){

  }

  createAccessLogFileAndWrite(text: string) {
      this.file.checkFile(this.file.dataDirectory, 'access.log')
      .then(doesExist => { //File exists

          console.log("doesExist : " + doesExist);
          console.log(this.file.dataDirectory);
          return this.writeExistingFile(text); //add new text to log

      }).catch(err => { //File doesn't exists
          console.log("NON C'E");
          return this.file.createFile(this.file.dataDirectory, 'access.log', false)
              .then(FileEntry => this.writeToAccessLogFile(text))
              .catch(err => console.log('Couldnt create file'));

      });

  }

  writeToAccessLogFile(text: string) {
      this.file.writeExistingFile(this.file.dataDirectory, 'access.log', text);
  }

  writeExistingFile(text){

    this.file.readAsText(this.file.dataDirectory, 'access.log').then((content)=>{
      let newContent = content;
      newContent = content + "<br>" + text;

      this.file.writeExistingFile(this.file.dataDirectory, 'access.log', newContent);

    });

  }

  getDate(){
    let currentdate = new Date();
    let datetime = currentdate.getDate() + "/"
          + (currentdate.getMonth()+1)  + "/"
          + currentdate.getFullYear() + " @ "
          + currentdate.getHours() + ":"
          + currentdate.getMinutes() + ":"
          + currentdate.getSeconds();

    return datetime;
  }

}
