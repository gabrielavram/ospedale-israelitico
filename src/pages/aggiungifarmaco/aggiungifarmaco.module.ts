import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AggiungifarmacoPage } from './aggiungifarmaco';

@NgModule({
  declarations: [
    AggiungifarmacoPage,
  ],
  imports: [
    IonicPageModule.forChild(AggiungifarmacoPage),
  ],
})
export class AggiungifarmacoPageModule {}
