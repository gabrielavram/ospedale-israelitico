import { Component } from '@angular/core';

import { AlertController } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { FileLogger } from '../../filelogger';

import { HomePage } from '../home/home';
import { AggiungifarmacoPage } from '../aggiungifarmaco/aggiungifarmaco';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AggiungifarmacoPage;

  constructor(
    private callCtrl: CallNumber,
    private alertCtrl: AlertController,
    private filelogger: FileLogger
  ) {

  }

  chiamaCup(){
    let number = "1234";
    let alert = this.alertCtrl.create({
      title: 'Vuoi chiamare il CUP?',
      buttons: [
      {
        text: 'Annulla',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Chiama',
        handler: () => {

          this.filelogger.createAccessLogFileAndWrite(this.filelogger.getDate() + ": [TabsPage]: Chiamata effettuata"); //log di chiamata

          this.callCtrl.callNumber(number, true)
          .then(res => console.log('Launched dialer!', res))
          .catch(err => console.log('Error launching dialer', err));
        }
      }
    ]
    });
    alert.present();
  }
}
