import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { IcentriPage } from '../icentri/icentri';

@IonicPage()
@Component({
  selector: 'page-specialista',
  templateUrl: 'specialista.html',
})
export class SpecialistaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SpecialistaPage');
  }

  pushIcentri(id){
    this.navCtrl.push(IcentriPage, {
      focusOnMarker: id
    });
  }

}
