import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpecialistaPage } from './specialista';

@NgModule({
  declarations: [
    SpecialistaPage,
  ],
  imports: [
    IonicPageModule.forChild(SpecialistaPage),
  ],
})
export class SpecialistaPageModule {}
