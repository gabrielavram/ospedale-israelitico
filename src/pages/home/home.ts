import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { LamiaterapiaPage } from '../lamiaterapia/lamiaterapia';
import { SpecialistiPage } from '../specialisti/specialisti';
import { NewsPage } from '../news/news';
import { IcentriPage } from '../icentri/icentri';
import { InostriserviziPage } from '../inostriservizi/inostriservizi';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  pushLamiaterapia(){
    this.navCtrl.push(LamiaterapiaPage);
  }
  pushSpecialisti(){
    this.navCtrl.push(SpecialistiPage);
  }
  pushNews(){
    this.navCtrl.push(NewsPage);
  }
  pushIcentri(){
    this.navCtrl.push(IcentriPage)
  }
  pushInostriservizi(){
    this.navCtrl.push(InostriserviziPage);
  }

}
