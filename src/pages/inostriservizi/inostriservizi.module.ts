import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InostriserviziPage } from './inostriservizi';

@NgModule({
  declarations: [
    InostriserviziPage,
  ],
  imports: [
    IonicPageModule.forChild(InostriserviziPage),
  ],
})
export class InostriserviziPageModule {}
