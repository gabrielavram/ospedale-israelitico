import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { CallNumber } from '@ionic-native/call-number';

import { Notifications } from '../../notifications';
import { NotificationFunctions } from '../../notificationfunctions';


 declare var cordova;

@IonicPage()
@Component({
  selector: 'page-inostriservizi',
  templateUrl: 'inostriservizi.html',
})
export class InostriserviziPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private callCtrl: CallNumber,
    private notifications: Notifications
  ) {
    NotificationFunctions.inizialize_navCtrl(this.navCtrl);
    NotificationFunctions.inizialize_alertCtrl(this.alertCtrl);
    NotificationFunctions.inizialize_cordova(cordova);
  }

  ionViewDidLoad() {
    this.notifications.run();
  }

  //Dottori
  dottori = [
    {
      type:'radio',
      label:'Non ho preferenze',
      value:'Non ho preferenze',
    },
    {
      type:'radio',
      label:'Tizio',
      value:'Tizio'
    },
    {
      type:'radio',
      label:'Caoio',
      value:'Caoio'
    },
  ];

  testoSelezionaDottore = "Seleziona";

  //Domande
  idCont = 1;
  questionsDefault = [
    { id: 0, question: "Vuoi prenotare una visita?", yesB: false, noB: false },
    { id: 1, question: "Sai già il nome del dottore?", yesB: false, noB: false },
    { id: 2, question: "Sai già il nome del dottore?", yesB: false, noB: false },
    { id: 3, question: "Prova prova?", yesB: false, noB: false },

  ];

  questionsShowing = [
    { id: 0, question: "Vuoi prenotare una visita?", yesB: false, noB: false }
  ];






  addNewQuestion(){
    this.questionsShowing.push(this.questionsDefault[this.idCont]);
    this.idCont++;
  }

  selezionaDottore(){
    let prompt = this.alertCtrl.create({
    message: 'Seleziona il dottore ',
    inputs : this.dottori,
    buttons : [
    {
        text: "Annulla",
        role: 'cancel',
        handler: data => {

        }
    },
    {
        text: "Seleziona",
        handler: data => {
          this.testoSelezionaDottore = data;

        }
    }]});
    prompt.present();
  }

  call(){
    this.callCtrl.callNumber("3274956477", true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }

}
