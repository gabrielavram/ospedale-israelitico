import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IcentriPage } from './icentri';

@NgModule({
  declarations: [
    IcentriPage,
  ],
  imports: [
    IonicPageModule.forChild(IcentriPage),
  ],
})
export class IcentriPageModule {}
