import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { CallNumber } from '@ionic-native/call-number';
import { GoogleMaps, GoogleMap, GoogleMapsEvent, Marker, GoogleMapOptions, CameraPosition, } from '@ionic-native/google-maps';
import { FileLogger } from '../../filelogger';

@IonicPage()
@Component({
  selector: 'page-icentri',
  templateUrl: 'icentri.html'
})
export class IcentriPage {
  @ViewChild('content') content:any;

  markersArray = [
    { title: 'Via Fulda, 14', lat: 41.8355407, lng: 12.427734600000008},
    { title: 'Via Giuseppe Veronese, 53-59', lat: 41.8663761, lng: 12.46449419999999},
    { title: 'P.zza San Bartolomeo all\'isola, 21', lat: 41.890587, lng: 12.478106000000025}
  ];
  markers = [];
  map: GoogleMap;

  focusOnMarker;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private callCtrl: CallNumber,
    private alertCtrl: AlertController,
    private filelogger: FileLogger
  ) {
    this.focusOnMarker = navParams.get('focusOnMarker');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IcentriPage');
    this.loadMap();
  }


  loadMap() {

    let mapOptions: GoogleMapOptions = {
      camera: {
         target: {
           lat: 41.901809,
           lng: 12.491477
         },
         zoom: 10,
         tilt: 0
       }
    };

    // Create a map after the view is ready and the native platform is ready.
    this.map = GoogleMaps.create('map_canvas', mapOptions);
    this.filelogger.createAccessLogFileAndWrite(this.filelogger.getDate() + ": [IcentriPage]: Mappa caricata"); //log di mappa


    //caricamento markers
    for( let i=0; i<this.markersArray.length; i++){
      this.map.addMarker({
        position: {lat: this.markersArray[i].lat, lng: this.markersArray[i].lng},
        title: this.markersArray[i].title
      }).then((marker:Marker) => {
        this.markers.push(marker);
        marker.showInfoWindow();
        if(i == 2){
          this.markersLoaded();
        }
      });
    }
    this.filelogger.createAccessLogFileAndWrite(this.filelogger.getDate() + ": [IcentriPage]: Markers mappa caricati"); //log di markers

    // No longer wait GoogleMapsEvent.MAP_READY event
    // ( except you use map.getVisibleRegion() )
  }

  markersLoaded(){
    if(this.focusOnMarker != undefined){
      this.showMarker(this.focusOnMarker);
    }
  }

  showMarker(id){
    this.content.scrollToBottom(300);
    this.markers[id].showInfoWindow();
  }

  call(number: string){
    let alert = this.alertCtrl.create({
      title: 'Vuoi chiamare l\'ospedale?',
      buttons: [
      {
        text: 'Annulla',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Chiama',
        handler: () => {
          this.filelogger.createAccessLogFileAndWrite(this.filelogger.getDate() + ": [IcentriPage]: Chiamata effettuata"); //log di chiamata

          this.callCtrl.callNumber(number, true)
          .then(res => console.log('Launched dialer!', res))
          .catch(err => console.log('Error launching dialer', err));
        }
      }
    ]
    });
    alert.present();
  }


}
