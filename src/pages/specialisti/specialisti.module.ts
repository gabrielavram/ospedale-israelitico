import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpecialistiPage } from './specialisti';

@NgModule({
  declarations: [
    SpecialistiPage,
  ],
  imports: [
    IonicPageModule.forChild(SpecialistiPage),
  ],
})
export class SpecialistiPageModule {}
