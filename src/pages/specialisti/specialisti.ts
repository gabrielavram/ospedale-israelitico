import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { SpecialistaPage } from '../specialista/specialista';

@IonicPage()
@Component({
  selector: 'page-specialisti',
  templateUrl: 'specialisti.html',
})
export class SpecialistiPage {

  searchQuery: string = '';
  items: string[];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.initializeItems();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SpecialistiPage');
  }

  pushSpecialistaPage(){
    this.navCtrl.push(SpecialistaPage);
  }

  initializeItems() {
    this.items = [
      'Marco Rossi',
      'Sara Rossi',
      'Giovanni Russo',
      'Paolo Giallo'
    ];
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

}
