import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LamiaterapiaPage } from './lamiaterapia';

@NgModule({
  declarations: [
    LamiaterapiaPage,
  ],
  imports: [
    IonicPageModule.forChild(LamiaterapiaPage),
  ],
})
export class LamiaterapiaPageModule {}
