import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, PopoverController } from 'ionic-angular';

import { PopoverPage } from './popover';

import { Notifications } from '../../notifications';
import { NotificationFunctions } from '../../notificationfunctions';

import { FileLogger } from '../../filelogger';


//declare var cordova;

@IonicPage()
@Component({
  selector: 'page-lamiaterapia',
  templateUrl: 'lamiaterapia.html',
})
export class LamiaterapiaPage {

  text;

  form = { title: '', field: '', date: '', time: '' };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private notifications: Notifications,
    private filelogger: FileLogger,
    public popoverCtrl: PopoverController
  ) {
    NotificationFunctions.inizialize_navCtrl(this.navCtrl);
    NotificationFunctions.inizialize_alertCtrl(this.alertCtrl);
    //NotificationFunctions.inizialize_cordova(cordova);
  }

  ionViewDidLoad() {
    console.log("VIEW LOADED");
    this.filelogger.createAccessLogFileAndWrite("ddddd");

  }

  submitForm(){

    let dateChanged = this.form.date.split("-");
    let timeChanged = this.form.time.split(":");
    console.log("DATE PRIMA *****" + this.form.date);
    console.log("DATE --- " + dateChanged);
    console.log("TIME --- " + timeChanged);

    this.notifications.scheduleNotification(dateChanged, timeChanged);

  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverPage);
    popover.present({
      ev: myEvent
    });
  }

  //textarray = ["prova 1", "\ prova 2"];
  //re = /, /gi;




}
