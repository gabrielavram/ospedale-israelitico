import { NavController, Tabs, AlertController } from 'ionic-angular';

export class NotificationFunctions{

  private static nav;
  private static alert;
  private static cordova;

  public static getNav(){
    return NotificationFunctions.nav;
  }
  public static getAlert(){
    return NotificationFunctions.alert;
  }
  public static getCordova(){
    return NotificationFunctions.cordova;
  }

  public static inizialize_navCtrl(ctrl){
    NotificationFunctions.nav = ctrl;
  }

  public static inizialize_alertCtrl(ctrl){
    NotificationFunctions.alert = ctrl;
  }

  public static inizialize_cordova(ctrl){
    NotificationFunctions.cordova = ctrl;
  }

  public static newNotification(){
    console.log("ENTRATOOOOOOO");
    var data = new Date();
    data.setMinutes(data.getMinutes()+1);
    NotificationFunctions.cordova.plugins.notification.local.schedule({
        title: 'Notification after that ',
        trigger: { at: data }
    });

  }

}
