import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

//Plugins
import { CallNumber } from '@ionic-native/call-number';
import { Notifications } from '../notifications';
import { FileLogger } from '../filelogger';
import { File } from '@ionic-native/file';
import { GoogleMaps } from "@ionic-native/google-maps";

import { TabsPage } from '../pages/tabs/tabs';

//Pages
import { HomePage } from '../pages/home/home';
import { AggiungifarmacoPage } from '../pages/aggiungifarmaco/aggiungifarmaco';
//Pages from  home
import { LamiaterapiaPage } from '../pages/lamiaterapia/lamiaterapia';
import { SpecialistiPage } from '../pages/specialisti/specialisti';
import { SpecialistaPage } from '../pages/specialista/specialista';
import { NewsPage } from '../pages/news/news';
import { IcentriPage } from '../pages/icentri/icentri';
import { InostriserviziPage } from '../pages/inostriservizi/inostriservizi';

import { PopoverPage } from '../pages/lamiaterapia/popover';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    IcentriPage,
    InostriserviziPage,
    AggiungifarmacoPage,
    LamiaterapiaPage,
    SpecialistiPage,
    SpecialistaPage,
    NewsPage,
    PopoverPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: '',
      iconMode: 'ios',
      modalEnter: 'modal-slide-in',
      modalLeave: 'modal-slide-out',
      tabsPlacement: 'bottom',
      pageTransition: 'ios-transition'
    }
  )],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    IcentriPage,
    InostriserviziPage,
    AggiungifarmacoPage,
    LamiaterapiaPage,
    SpecialistiPage,
    SpecialistaPage,
    NewsPage,
    PopoverPage,
    TabsPage
  ],
  providers: [
    Notifications,
    FileLogger,
    StatusBar,
    SplashScreen,
    CallNumber,
    File,
    GoogleMaps,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
